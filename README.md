
## Installation

Run the following commands

```bash
  git clone https://gitlab.com/akhilPremkishore/csv-import.git
  Run: 
    composer install
```
  If its not working try
```bash
    composer require maatwebsite/excel --ignore-platform-reqs 
```
```bash
    php artisan key:generate
    php artisan migrate
    php artisan serve 
```

CSV file is attached with the mail.
