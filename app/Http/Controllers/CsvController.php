<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\CompanyImport;

class CsvController extends Controller
{

    public function store(Request $request): RedirectResponse
    {

        $request->validate([
            'csv' => 'required|mimes:csv,txt'
        ]);

        // ini_set('max_execution_time', 0); // 0 = Unlimited
        // Excel::import(new CompanyImport, request()->file('csv'));

        /*
            Write Code Here for
            Store $imageName name in DATABASE from HERE
        */

        return back()
                    ->with('success', 'You have successfully import csv.');
    }
}
