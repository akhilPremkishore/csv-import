<?php

namespace App\Imports;

use App\Models\Company;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithBatchInserts;

class CompanyImport implements ToModel, WithStartRow, WithChunkReading, WithBatchInserts
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Company([
            'organization_id' => $row[1],
            'name' => $row[2],
            'website' => $row[3],
            'country' => $row[4],
            'description' => $row[5],
            'founded' => $row[6],
            'industry' => $row[7],
            'no_of_employees' => $row[8]
        ]);
    }

     /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    public function chunkSize(): int
    {
        return 10000;
    }

    public function batchSize(): int
    {
        return 5000;
    }
}
