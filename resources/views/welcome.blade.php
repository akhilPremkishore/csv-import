<!DOCTYPE html>
<html>
<head>
    <title>CSV file import</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
<div class="container">

    <div class="panel panel-primary">

      <div class="panel-heading">
        <h2>CSV file import</h2>
      </div>

      <div class="panel-body">

        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
        </div>
        @endif

        <form action="{{ route('csv.store') }}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="mb-3">
                <label class="form-label" for="inputImage">Csv file:</label>
                <input
                    type="file"
                    name="csv"
                    id="inputCsv"
                    class="form-control @error('csv') is-invalid @enderror">

                @error('csv')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>

            <div class="mb-3">
                <button type="submit" class="btn btn-success">Upload</button>
            </div>

        </form>

      </div>
    </div>
</div>
</body>

</html>
